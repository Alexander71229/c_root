using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BarraVida:MonoBehaviour{
	public int equipo;
	public int maximo=100;
	public float actual=100;
	void Start(){
		if(GetComponent<BoxCollider2D>()==null){
			gameObject.AddComponent<BoxCollider2D>();
		}
	}
	public void modificar(float v){
		actual+=v;
		if(actual>100){
			actual=100;
		}
		if(actual<0){
			actual=0;
		}
		if(actual==0){
			Destroy(gameObject);
		}
	}
}
