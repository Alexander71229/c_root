using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DamageZone:MonoBehaviour{
	public int equipo;
	public int damage=1;
	void OnTriggerEnter2D(Collider2D c){
		if(c.gameObject.GetComponent<BarraVida>()!=null&&c.gameObject.GetComponent<BarraVida>().equipo!=equipo){
			c.gameObject.GetComponent<BarraVida>().modificar(-damage);
			Destroy(gameObject);
		}
	}
}
