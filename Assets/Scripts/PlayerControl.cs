using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerControl:MonoBehaviour{
	void Start(){
		if(GetComponent<Rigidbody2D>()==null){
			gameObject.AddComponent<Rigidbody2D>();
			GetComponent<Rigidbody2D>().gravityScale=0;
		}
		if(GetComponent<BoxCollider2D>()==null){
			gameObject.AddComponent<BoxCollider2D>();
		}
		gameObject.tag="Player";
	}
	void Update(){
		GetComponent<Rigidbody2D>().AddForce(new Vector2(Input.GetAxis("Horizontal")*5.2f,Input.GetAxis("Vertical")*0f)-GetComponent<Rigidbody2D>().velocity*.2f);
	}
}
